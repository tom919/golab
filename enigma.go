package main

import (
	"bufio"
	"crypto/sha256"
	"crypto/sha512"
	b64 "encoding/base64"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"

	"golang.org/x/crypto/ripemd160"
)

func main() {

	var vinput, ops string
	var vinputb string
	// var next int
	fmt.Println("messages:")
	fmt.Scanf("%s", &vinput)

	fmt.Println("messages byte:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	vinputb = scanner.Text()

	fmt.Println("select operation:")
	fmt.Scanf("%s", &ops)

	switch ops {
	case "sha256":
		out := s256(vinput)
		fmt.Printf("%v\n", out)
	case "sha512":
		out := s512(vinput)
		fmt.Printf("%v\n", out)
	case "ripemd":
		out := ripemd(vinput)
		fmt.Printf("%v\n", out)
	case "en1":
		out := cEncrypt(vinput)
		fmt.Printf("%v\n", out)
	case "de1":
		out := cDecrypt(vinputb)
		fmt.Printf("%v\n", out)
	default:
		fmt.Printf("ops not supported")
	}
}

func s256(input string) string {
	h := sha256.New()
	h.Write([]byte(input))
	outputByte := h.Sum(nil)
	outputString := b64.URLEncoding.EncodeToString(outputByte)
	return outputString
}

func s512(input string) string {
	h := sha512.New()
	h.Write([]byte(input))
	outputByte := h.Sum(nil)
	outputString := b64.URLEncoding.EncodeToString(outputByte)
	return outputString
}

func ripemd(input string) string {
	h := ripemd160.New()
	h.Write([]byte(input))
	outputByte := h.Sum(nil)
	outputString := b64.URLEncoding.EncodeToString(outputByte)
	return outputString
}

func cEncrypt(input string) []byte {
	var outputTempASCII int
	var output []rune
	var outChar []string
	output = []rune(input)
	outputLength := len(output)

	for i := 0; i < outputLength; i++ {
		outputTempASCII = int(output[i]) + 1
		outChar = append(outChar, string(outputTempASCII), string(107+i))

	}
	encryptedMessages := strings.Join(outChar, "")
	encryptedMessagesByte := []byte(reverse(encryptedMessages))
	return encryptedMessagesByte
}

func cDecrypt(inputB string) string {

	sa := strings.Split(inputB, " ")

	var t2 = []string{}
	var jt int
	var jx string
	var x int
	x = 0
	for _, i := range sa {
		j, err := strconv.Atoi(i)
		jt = j - 1
		jx = string(jt)
		if err != nil {
			panic(err)
		}

		if x%2 != 0 {
			t2 = append(t2, jx)
		} else {

		}
		x++

	}

	decryptedMessages := reverse(strings.Join(t2, ""))
	return decryptedMessages
}

func reverse(s string) string {
	size := len(s)
	buf := make([]byte, size)
	for start := 0; start < size; {
		r, n := utf8.DecodeRuneInString(s[start:])
		start += n
		utf8.EncodeRune(buf[size-start:], r)
	}
	return string(buf)
}

func sliceAtoi(sa []string) ([]int, error) {
	si := make([]int, 0, len(sa))
	for _, a := range sa {
		i, err := strconv.Atoi(a)
		if err != nil {
			return si, err
		}
		si = append(si, i)
	}
	return si, nil
}
