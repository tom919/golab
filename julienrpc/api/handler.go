package api

import (
	"log"

	"golang.org/x/net/context"
)

//Server represent the grpc server
type Server struct {
}

//SayHello generates response
func (s *Server) SayHello(ctx context.Context, in *PingMessage) (*PingMessage, error) {
	log.Printf("receive messages %s", in.Greeting)
	return &PingMessage{Greeting: "hi, your messages received"}, nil
}
