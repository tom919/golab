package main

import (
	"bytes"
	"crypto/sha256"
	"testing"
)

//TestContent struct
type TestVar struct {
	x string
}

//CalculateHash hashes the values of a TestContent
func (t TestVar) CalculateHash() ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write([]byte(t.x)); err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}

//Equals tests for equality of two Contents
func (t TestVar) Equals(other Content) (bool, error) {
	return t.x == other.(TestVar).x, nil
}

var table = []struct {
	contents     []Content
	expectedHash []byte
}{
	{
		contents: []Content{
			TestVar{
				x: "3e55352e649cb04ce701b6046d27163cf849a9fe73396ff072036ad7d0186ee3",
			},
			TestVar{
				x: "28789ecbad8bfe7fb0eeabebbfd6bb96f1eea48ac63f8e80a7d039b52d79f85a",
			},
			TestVar{
				x: "463a2de540ea69dfe8bfc9201eec511e4fcaeb5dafbbc6167c773371d206febb",
			},
			TestVar{
				x: "f1aa5cf0f1f28e8bd46d31b45f48cb3dbb405cffb8533a3647210982618a8e57",
			},
			TestVar{
				x: "db4b0f506b85c867e995c91ec9ac3a77cae374289208452a58838152dc3502cf",
			},
			TestVar{
				x: "2d46429f02dc81e2187031af9070c21570efc26b4a5b127c04774f5a6acc043f",
			},
			TestVar{
				x: "0ee15feb5d7b018d43d3cf8c918f388da3bddb5bd3d2c71e920e2cd144e0aa31",
			},
			TestVar{
				x: "73f3573e063a936e7fd3f410fe36c5a52bac1a7b113155b8c6b332626d41aa61",
			},
		},
		expectedHash: []byte{76, 190, 133, 199, 105, 131, 156, 92, 19, 148,
			223, 173, 129, 47, 62, 141, 22, 180, 249, 64, 105, 246, 110, 120, 57, 206,
			233, 181, 91, 177, 251, 158},
	},
}

var table2 = []struct {
	content      string
	leafHash     []byte
	intermedHash []byte
	nodeHash     []byte
	expectedHash []byte
}{
	{
		content: "3e55352e649cb04ce701b6046d27163cf849a9fe73396ff072036ad7d0186ee3",
		leafHash: []byte{203, 33, 98, 252, 48, 232, 182, 33, 10, 220, 153, 63, 148, 150,
			155, 61, 48, 229, 124, 232, 43, 174, 194, 62, 101, 215, 48, 235, 202, 195, 222, 57},
		intermedHash: []byte{177, 247, 10, 191, 42, 44, 93, 250, 236, 151, 196, 144, 127, 107,
			206, 140, 106, 22, 87, 207, 96, 0, 45, 202, 74, 40, 55, 40, 221, 151, 113, 202},
		nodeHash: []byte{96, 160, 130, 169, 133, 176, 42, 86, 179, 124, 138, 146, 82, 150,
			166, 151, 201, 187, 88, 235, 131, 135, 59, 180, 28, 159, 119, 164, 93, 45, 181, 179},
		expectedHash: []byte{76, 190, 133, 199, 105, 131, 156, 92, 19, 148, 223, 173, 129,
			47, 62, 141, 22, 180, 249, 64, 105, 246, 110, 120, 57, 206, 233, 181, 91, 177, 251, 158},
	},
}

func TestNewTree(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i].expectedHash, tree.MerkleRoot())
		}
	}
}

func BenchmarkMerkleTree(b *testing.B) {
	// run the merkle tree bench
	for n := 0; n < 50000000; n++ {
		for i := 0; i < len(table); i++ {
			tree, err := NewTree(table[i].contents)
			if err != nil {
				b.Error("error: unexpected error:  ", err)
			}
			if bytes.Compare(tree.MerkleRoot(), table[i].expectedHash) != 0 {
				b.Errorf("error: expected hash equal to %v got %v", table[i].expectedHash, tree.MerkleRoot())
			}
		}
	}
}

func Test_MerkleRoot(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i].expectedHash, tree.MerkleRoot())
		}
	}
}

func Test_RebuildTree(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		err = tree.RebuildTree()
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i].expectedHash, tree.MerkleRoot())
		}
	}
}

func Test_RebuildTreeWith(t *testing.T) {
	for i := 0; i < len(table)-1; i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		err = tree.RebuildTreeWith(table[i+1].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i+1].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i+1].expectedHash, tree.MerkleRoot())
		}
	}
}

func Test_VerifyTree(t *testing.T) {
	for i := 0; i < len(table); i++ {

		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}

		v1, err := tree.VerifyTree()
		if err != nil {
			t.Fatal(err)
		}
		if v1 != true {
			t.Error("error: expected tree to be valid")
		}

		tree.Root.Hash = []byte{1}
		tree.merkleRoot = []byte{1}
		v2, err := tree.VerifyTree()
		if err != nil {
			t.Fatal(err)
		}
		if v2 != false {
			t.Error("error: expected tree to be invalid")
		}
	}
}

func Test_VerifyTree2(t *testing.T) {
	for i := 0; i < len(table); i++ {

		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}

		v1, err := tree.VerifyTree2(table2[i].content, table2[i].leafHash, table2[i].intermedHash, table2[i].nodeHash)
		if err != nil {
			t.Fatal(err)
		}
		if v1 != true {
			t.Error("error: expected tree to be valid")
		}
	}
}

func Test_VerifyContent(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if len(table[i].contents) > 0 {
			v, err := tree.VerifyContent(table[i].contents[0])
			if err != nil {
				t.Fatal(err)
			}
			if !v {
				t.Error("error: expected valid content")
			}
		}
		if len(table[i].contents) > 1 {
			v, err := tree.VerifyContent(table[i].contents[1])
			if err != nil {
				t.Fatal(err)
			}
			if !v {
				t.Error("error: expected valid content")
			}
		}
		if len(table[i].contents) > 2 {
			v, err := tree.VerifyContent(table[i].contents[2])
			if err != nil {
				t.Fatal(err)
			}
			if !v {
				t.Error("error: expected valid content")
			}
		}
		if len(table[i].contents) > 0 {
			tree.Root.Hash = []byte{1}
			tree.merkleRoot = []byte{1}
			v, err := tree.VerifyContent(table[i].contents[0])
			if err != nil {
				t.Fatal(err)
			}
			if v {
				t.Error("error: expected invalid content")
			}
			if err := tree.RebuildTree(); err != nil {
				t.Fatal(err)
			}
		}
		v, err := tree.VerifyContent(TestVar{x: "NotInTestTable"})
		if err != nil {
			t.Fatal(err)
		}
		if v {
			t.Error("error: expected invalid content")
		}
	}
}

func Test_String(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if tree.String() == "" {
			t.Error("error: expected not empty string")
		}
	}
}

func Test_MerklePath(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		for j := 0; j < len(table[i].contents); j++ {
			merklePath, index, _ := tree.GetMerklePath(table[i].contents[j])

			hash, err := tree.Leafs[j].calculateNodeHash()
			if err != nil {
				t.Error("error: calculateNodeHash error:  ", err)
			}
			h := sha256.New()
			for k := 0; k < len(merklePath); k++ {
				if index[k] == 1 {
					hash = append(hash, merklePath[k]...)
				} else {
					hash = append(merklePath[k], hash...)
				}
				if _, err := h.Write(hash); err != nil {
					t.Error("error: Write error:  ", err)
				}
				hash, err = calHash(hash)
				if err != nil {
					t.Error("error: calHash error:  ", err)
				}
			}
			if bytes.Compare(tree.MerkleRoot(), hash) != 0 {
				t.Errorf("error: expected hash equal to %v got %v", hash, tree.MerkleRoot())
			}
		}
	}
}
