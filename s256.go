package main

import (
	"crypto/sha256"
	"fmt"
)

func main() {
	// var vinput string
	// var vinput, vinput2, vinput3, vinput4, vinput5, vinput6, vinput7, vinput8 string

	// fmt.Println("messages:")
	// vinput = "3e55352e649cb04ce701b6046d27163cf849a9fe73396ff072036ad7d0186ee3"
	// vinput2 = "28789ecbad8bfe7fb0eeabebbfd6bb96f1eea48ac63f8e80a7d039b52d79f85a"
	// vinput3 = "463a2de540ea69dfe8bfc9201eec511e4fcaeb5dafbbc6167c773371d206febb"
	// vinput4 = "f1aa5cf0f1f28e8bd46d31b45f48cb3dbb405cffb8533a3647210982618a8e57"
	// vinput5 = "db4b0f506b85c867e995c91ec9ac3a77cae374289208452a58838152dc3502cf"
	// vinput6 = "2d46429f02dc81e2187031af9070c21570efc26b4a5b127c04774f5a6acc043f"
	// vinput7 = "0ee15feb5d7b018d43d3cf8c918f388da3bddb5bd3d2c71e920e2cd144e0aa31"
	// vinput8 = "73f3573e063a936e7fd3f410fe36c5a52bac1a7b113155b8c6b332626d41aa61"
	hash1 := []byte{204, 228, 26, 203, 194, 236, 194, 10, 64, 223, 54, 30, 161, 251, 247, 204, 219, 246, 22, 80,
		141, 239, 248, 164, 78, 150, 28, 201, 200, 76, 9, 134}
	hash2 := []byte{203, 33, 98, 252, 48, 232, 182, 33, 10, 220, 153, 63, 148, 150, 155, 61,
		48, 229, 124, 232, 43, 174, 194, 62, 101, 215, 48, 235, 202, 195, 222, 57}
	hash3 := []byte{211, 31, 194, 225, 72, 58, 140, 85, 132, 249, 138, 154, 10, 179, 10, 124,
		215, 122, 197, 113, 218, 223, 254, 166, 237, 92, 13, 143, 138, 28, 209, 22}
	hash4 := []byte{62, 80, 159, 12, 198, 20, 141, 187, 102, 86, 71, 239, 161, 87, 60, 232,
		169, 98, 5, 111, 60, 198, 142, 234, 182, 248, 42, 41, 32, 38, 35, 14}

	hash5 := []byte{240, 97, 10, 240, 65, 14, 81, 112, 38, 21, 197, 7, 112, 137, 211, 1, 162, 117,
		234, 115, 159, 201, 85, 62, 213, 62, 102, 75, 136, 153, 20}
	hash6 := []byte{202, 51, 137, 48, 92, 117, 43, 141, 116, 245, 76, 36, 112, 193, 26, 252, 85,
		104, 210, 158, 153, 138, 122, 31, 247, 87, 16, 91, 48, 200, 11, 170}
	hash7 := []byte{184, 244, 158, 71, 215, 71, 114, 95, 204, 20, 52, 39, 88, 144, 22, 29, 143,
		83, 24, 26, 2, 106, 247, 170, 105, 197, 68, 3, 176, 214, 52, 64}
	hash8 := []byte{154, 153, 100, 5, 245, 101, 158, 0, 185, 231, 255, 225, 96, 244, 64, 90,
		174, 185, 201, 105, 181, 157, 242, 192, 53, 231, 33, 211, 250, 141, 249, 159}

	// h := sha256.New()
	// h.Write([]byte(vinput))
	// outputByte1 := h.Sum(nil)

	// h1 := sha256.New()
	// h1.Write([]byte(vinput2))
	// outputByte2 := h1.Sum(nil)

	// h2 := sha256.New()
	// h2.Write([]byte(vinput3))
	// outputByte3 := h2.Sum(nil)

	// h3 := sha256.New()
	// h3.Write([]byte(vinput4))
	// outputByte4 := h3.Sum(nil)

	// h4 := sha256.New()
	// h4.Write([]byte(vinput5))
	// outputByte5 := h4.Sum(nil)

	// h5 := sha256.New()
	// h5.Write([]byte(vinput6))
	// outputByte6 := h5.Sum(nil)

	// h6 := sha256.New()
	// h6.Write([]byte(vinput7))
	// outputByte7 := h6.Sum(nil)

	// h7 := sha256.New()
	// h7.Write([]byte(vinput8))
	// outputByte8 := h7.Sum(nil)
	// outputString := b64.URLEncoding.EncodeToString(outputByte)
	// fmt.Printf("calculatedLeaf1: %v\n", outputByte1)
	// fmt.Printf("calculatedLeaf2: %v\n", outputByte2)
	// fmt.Printf("calculatedLeaf3: %v\n", outputByte3)
	// fmt.Printf("calculatedLeaf4: %v\n", outputByte4)
	// fmt.Printf("calculatedLeaf5: %v\n", outputByte5)
	// fmt.Printf("calculatedLeaf6: %v\n", outputByte6)
	// fmt.Printf("calculatedLeaf7: %v\n", outputByte7)
	// fmt.Printf("calculatedLeaf8: %v\n", outputByte8)

	hIntermed1 := sha256.New()
	hIntermed1.Write(append(hash1, hash2...))
	intermedHash1 := hIntermed1.Sum(nil)

	hIntermed2 := sha256.New()
	hIntermed2.Write(append(hash3, hash4...))
	intermedHash2 := hIntermed2.Sum(nil)

	hIntermed3 := sha256.New()
	hIntermed3.Write(append(hash5, hash6...))
	intermedHash3 := hIntermed3.Sum(nil)

	hIntermed4 := sha256.New()
	hIntermed4.Write(append(hash7, hash8...))
	intermedHash4 := hIntermed4.Sum(nil)

	// fmt.Printf("intermedhash1Leaf : %v\n", intermedHash1)
	// fmt.Printf("intermedhash2 : %v\n", intermedHash2)
	// fmt.Printf("intermedhash3 : %v\n", intermedHash3)
	// fmt.Printf("intermedhash4 : %v\n", intermedHash4)

	hNode1 := sha256.New()
	hNode1.Write(append(intermedHash1, intermedHash2...))
	hNode1x := hNode1.Sum(nil)

	hNode2 := sha256.New()
	hNode2.Write(append(intermedHash3, intermedHash4...))
	hNode2x := hNode2.Sum(nil)

	// fmt.Printf("hNode1x : %v\n", hNode1x)
	// fmt.Printf("hNode2x : %v\n", hNode2x)

	hRoot := sha256.New()
	hRoot.Write(append(hNode1x, hNode2x...))
	hRootX := hRoot.Sum(nil)

	fmt.Printf("hRoot : %v\n", hRootX)
}
