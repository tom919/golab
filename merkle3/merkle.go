package main

import (
	"crypto/sha256"
	"fmt"
)

var data []string
var dataOut []string

func merkleTree(data []string) (intermedHash1, intermedHash2, intermedHash3, intermedHash4, hNode1x, hNode2x, hRootX []byte) {

	dataLength := len(data)
	fmt.Printf("data length: %v\n", dataLength)

	for i := 0; i < dataLength; i++ {
		fmt.Printf("%v\n", data[i])

	}

	h0 := sha256.New()
	h0.Write([]byte(data[0]))
	ob0 := h0.Sum(nil)
	fmt.Printf("origin message :%v\n", data[0])
	// fmt.Printf("leaf1 : %v\n", ob0)

	h1 := sha256.New()
	h1.Write([]byte(data[1]))
	ob1 := h1.Sum(nil)
	fmt.Printf("leaf2 : %v\n", ob1)

	h2 := sha256.New()
	h2.Write([]byte(data[2]))
	ob2 := h2.Sum(nil)
	// fmt.Printf("leaf3 : %v\n", ob2)

	h3 := sha256.New()
	h3.Write([]byte(data[3]))
	ob3 := h3.Sum(nil)
	// fmt.Printf("leaf4 : %v\n", ob3)

	h4 := sha256.New()
	h4.Write([]byte(data[4]))
	ob4 := h4.Sum(nil)
	// fmt.Printf("leaf5 : %v\n", ob4)

	h5 := sha256.New()
	h5.Write([]byte(data[5]))
	ob5 := h5.Sum(nil)
	// fmt.Printf("leaf6 : %v\n", ob5)

	h6 := sha256.New()
	h6.Write([]byte(data[6]))
	ob6 := h6.Sum(nil)
	// fmt.Printf("leaf7 : %v\n", ob6)

	h7 := sha256.New()
	h7.Write([]byte(data[7]))
	ob7 := h7.Sum(nil)
	// fmt.Printf("leaf8 : %v\n", ob7)

	//intermed

	hIntermed1 := sha256.New()
	hIntermed1.Write(append(ob0, ob1...))
	intermedHash1 = hIntermed1.Sum(nil)

	hIntermed2 := sha256.New()
	hIntermed2.Write(append(ob2, ob3...))
	intermedHash2 = hIntermed2.Sum(nil)

	hIntermed3 := sha256.New()
	hIntermed3.Write(append(ob4, ob5...))
	intermedHash3 = hIntermed3.Sum(nil)

	hIntermed4 := sha256.New()
	hIntermed4.Write(append(ob6, ob7...))
	intermedHash4 = hIntermed4.Sum(nil)

	//node

	hNode1 := sha256.New()
	hNode1.Write(append(intermedHash1, intermedHash2...))
	hNode1x = hNode1.Sum(nil)

	hNode2 := sha256.New()
	hNode2.Write(append(intermedHash3, intermedHash4...))
	hNode2x = hNode2.Sum(nil)

	//root
	hRoot := sha256.New()
	hRoot.Write(append(hNode1x, hNode2x...))
	hRootX = hRoot.Sum(nil)

	// output := []byte{76, 190, 133, 199, 105, 131, 156, 92, 19, 148, 223, 173, 129,
	// 	47, 62, 141, 22, 180, 249, 64, 105, 246, 110, 120, 57, 206, 233, 181, 91, 177, 251, 158}
	return intermedHash1, intermedHash2, intermedHash3, intermedHash4, hNode1x, hNode2x, hRootX
}

func main() {
	data = []string{
		"3e55352e649cb04ce701b6046d27163cf849a9fe73396ff072036ad7d0186ee3",
		"28789ecbad8bfe7fb0eeabebbfd6bb96f1eea48ac63f8e80a7d039b52d79f85a",
		"463a2de540ea69dfe8bfc9201eec511e4fcaeb5dafbbc6167c773371d206febb",
		"f1aa5cf0f1f28e8bd46d31b45f48cb3dbb405cffb8533a3647210982618a8e57",
		"db4b0f506b85c867e995c91ec9ac3a77cae374289208452a58838152dc3502cf",
		"2d46429f02dc81e2187031af9070c21570efc26b4a5b127c04774f5a6acc043f",
		"0ee15feb5d7b018d43d3cf8c918f388da3bddb5bd3d2c71e920e2cd144e0aa31",
		"73f3573e063a936e7fd3f410fe36c5a52bac1a7b113155b8c6b332626d41aa61",
	}

	_, r2, _, _, _, hNode2x, hRootX := merkleTree(data)

	// fmt.Printf("r1 : %v\n", r1)
	fmt.Printf("r2 : %v\n", r2)
	// fmt.Printf("r3 : %v\n", r3)
	// fmt.Printf("r4 : %v\n", r4)
	// fmt.Printf("node 1 : %v\n", hNode1x)
	fmt.Printf("node 2 : %v\n", hNode2x)
	fmt.Printf("root : %v\n", hRootX)
}
