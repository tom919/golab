package main

import "fmt"

func main() {
	var x, y, v int

	for i := 0; i < 4; i++ {

		if i != 0 {
			x = v + 1
			y = x + 1
		} else {
			x = 0
			y = x + 1
		}

		fmt.Printf("x :%v\n", x)
		fmt.Printf("y :%v\n", y)
		fmt.Printf("----\n")
		v = x + 1
	}
}
