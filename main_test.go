package main

import (
	"testing"
)

func Test_calculate(t *testing.T) {
	if calculate(2) != 4 {
		t.Error("not excpected result")
	}
}

func Test_tableCalculates(t *testing.T) {
	var tests = []struct {
		input    int
		expected int
	}{
		{2, 4},
		{-1, 1},
		{0, 2},
		{-5, -3},
		{99999, 100001},
	}

	for _, test := range tests {
		if output := calculate(test.input); output != test.expected {
			t.Error("Test Failed: {} inputted, {} expected, received: {}", test.input, test.expected, output)
		}
	}
}
