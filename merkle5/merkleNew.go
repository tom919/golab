package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
)

//opt struct
type opt struct {
	dataPiece string
	Hash      []byte
}

type intermed struct {
	Hash []byte
}

type node struct {
	Hash []byte
}

type root struct {
	Hash []byte
}

func markleTree(c []opt) ([]byte, []byte, []byte, []byte) {
	var intermeds []intermed
	var nodes []node
	var roots []root
	var rootsOdd []root
	var pLength, n int

	n = len(c)
	k := n % 2
	// fmt.Printf("leaf length: %v\n", n)
	for g := 0; g < 4; g++ {

		switch g {
		case 0:
			pLength = n
		case 1:
			pLength = n / 2
		case 2:
			pLength = (n / 2) / 2
		case 3:
			pLength = ((n / 2) / 2) / 2
		}

		var x1, y1, v1 int
		for i := 0; i < pLength; i++ {
			if i != 0 {
				x1 = v1 + 1
				y1 = x1 + 1
			} else {
				x1 = 0
				y1 = x1 + 1
			}

			dataBuffer := sha256.New()
			switch g {
			case 0:
				dataBuffer.Write([]byte(c[i].dataPiece))
				c[i].Hash = dataBuffer.Sum(nil)
				// fmt.Printf("leaf: %v\n", c[1].Hash)
			case 1:
				dataBuffer.Write(append(c[x1].Hash, c[y1].Hash...))
				intermeds = append(intermeds, intermed{dataBuffer.Sum(nil)})
				// fmt.Printf("intermed: %v\n", intermeds[1].Hash)

			case 2:
				dataBuffer.Write(append(intermeds[x1].Hash, intermeds[y1].Hash...))
				nodes = append(nodes, node{dataBuffer.Sum(nil)})
				// fmt.Printf("node : %v\n", nodes[i].Hash)
			case 3:
				dataBuffer.Write(append(nodes[x1].Hash, nodes[y1].Hash...))
				roots = append(roots, root{dataBuffer.Sum(nil)})
				if k != 0 {
					dataBuffer.Write(append(roots[0].Hash, c[len(c)-1].dataPiece...))
					rootsOdd = append(rootsOdd, root{dataBuffer.Sum(nil)})
				}
				// fmt.Printf("root : %v\n", roots[i].Hash)
			}

			v1 = x1 + 1
		}
	}
	if k != 0 {
		// fmt.Printf("odd root : %v\n", rootsOdd[0].Hash)
		return c[1].Hash, intermeds[1].Hash, nodes[1].Hash, rootsOdd[0].Hash
	}
	return c[1].Hash, intermeds[1].Hash, nodes[1].Hash, roots[0].Hash
}

// VerifyTree validates the hashes at each level
func VerifyTree(content string, leafHash, intermedHash, nodeHash, merkleRoot []byte) (bool, error) {
	//step1 calculate leaf
	hLeaf := sha256.New()
	hLeaf.Write([]byte(content))
	contentHash := hLeaf.Sum(nil)

	//step2 calculate intermed
	hIntermed := sha256.New()
	hIntermed.Write(append(contentHash, leafHash...))
	intermedHashLeft := hIntermed.Sum(nil)

	//step3 calculate node
	hNode := sha256.New()
	hNode.Write(append(intermedHashLeft, intermedHash...))
	nodeHashLeft := hNode.Sum(nil)
	//step4 calculate root
	hRoot := sha256.New()
	hRoot.Write(append(nodeHashLeft, nodeHash...))
	rootHash := hRoot.Sum(nil)
	// fmt.Printf("rootHash: %v\n", rootHash)
	if bytes.Compare(merkleRoot, rootHash) == 0 {
		return true, nil
	}
	return false, nil
}

func main() {

	var c = []opt{
		opt{
			dataPiece: "3e55352e649cb04ce701b6046d27163cf849a9fe73396ff072036ad7d0186ee3",
		},
		opt{
			dataPiece: "28789ecbad8bfe7fb0eeabebbfd6bb96f1eea48ac63f8e80a7d039b52d79f85a",
		},
		opt{
			dataPiece: "463a2de540ea69dfe8bfc9201eec511e4fcaeb5dafbbc6167c773371d206febb",
		},
		opt{
			dataPiece: "f1aa5cf0f1f28e8bd46d31b45f48cb3dbb405cffb8533a3647210982618a8e57",
		},
		opt{
			dataPiece: "db4b0f506b85c867e995c91ec9ac3a77cae374289208452a58838152dc3502cf",
		},
		opt{
			dataPiece: "2d46429f02dc81e2187031af9070c21570efc26b4a5b127c04774f5a6acc043f",
		},
		opt{
			dataPiece: "0ee15feb5d7b018d43d3cf8c918f388da3bddb5bd3d2c71e920e2cd144e0aa31",
		},
		opt{
			dataPiece: "73f3573e063a936e7fd3f410fe36c5a52bac1a7b113155b8c6b332626d41aa61",
		},
	}

	leafr, intermedr, noder, rootr := markleTree(c)

	fmt.Printf("leaf hash : %v\n", leafr)
	fmt.Printf("intermed hash : %v\n", intermedr)
	fmt.Printf("node hash : %v\n", noder)
	fmt.Printf("root hash : %v\n", rootr)

}
