package main

import (
	"fmt"
	"sync"
	"time"
)

func f(from string) {
	for i := 0; i < 3; i++ {
		fmt.Println(from, ":", i)
	}
}

func main() {
	var wg sync.WaitGroup

	wg.Add(3)
	f("direct")
	go f("goroutine")

	go func(msg string) {
		fmt.Println(msg)
	}("going")
	time.Sleep(1)
	fmt.Scanln()
	fmt.Println("done")

	wg.Wait()
}
