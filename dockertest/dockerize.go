package main

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
)

func main() {
	fmt.Println("Go Docker")

	fmt.Println("OS Detected: ", runtime.GOOS)
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("Mac OS")
	case "linux":
		fmt.Println("Linux Geek")
	default:
		fmt.Println("Other")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello World")
	})

	log.Fatal(http.ListenAndServe(":8081", nil))
}
