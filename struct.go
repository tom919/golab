package main

import "fmt"

type person struct {
	name string
	age  int
}

func main() {
	fmt.Println(person{"bob", 20})

	fmt.Println(person{name: "alice", age: 30})

	fmt.Println(person{name: "fred"})

	fmt.Println(&person{name: "ann", age: 50})
}
