package main

import (
	"bytes"
	"crypto/sha256"
	"testing"
)

//TestContent struct
type TestHash struct {
	x string
}

//CalculateHash hashes the values of a TestContent
func (t TestHash) CalculateHash() ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write([]byte(t.x)); err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}

//Equals tests for equality of two Contents
func (t TestHash) Equals(other Content) (bool, error) {
	return t.x == other.(TestHash).x, nil
}

var table = []struct {
	contents     []Content
	expectedHash []byte
}{
	{
		contents: []Content{
			TestHash{
				x: "3e55352e649cb04ce701b6046d27163cf849a9fe73396ff072036ad7d0186ee3",
			},
			TestHash{
				x: "28789ecbad8bfe7fb0eeabebbfd6bb96f1eea48ac63f8e80a7d039b52d79f85a",
			},
			TestHash{
				x: "463a2de540ea69dfe8bfc9201eec511e4fcaeb5dafbbc6167c773371d206febb",
			},
			TestHash{
				x: "f1aa5cf0f1f28e8bd46d31b45f48cb3dbb405cffb8533a3647210982618a8e57",
			},
		},
		expectedHash: []byte{54, 218, 35, 21, 133, 15, 82, 56, 2, 163, 0, 121, 9, 7, 48, 120, 110, 203,
			214, 148, 69, 145, 136, 246, 127, 189, 112, 48, 52, 22, 197, 90},
	},
}

func TestNewTree(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i].expectedHash, tree.MerkleRoot())
		}
	}
}

func Test_MerkleRoot(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i].expectedHash, tree.MerkleRoot())
		}
	}
}

func Test_RebuildTree(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		err = tree.RebuildTree()
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i].expectedHash, tree.MerkleRoot())
		}
	}
}

func Test_RebuildTreeWith(t *testing.T) {
	for i := 0; i < len(table)-1; i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		err = tree.RebuildTreeWith(table[i+1].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if bytes.Compare(tree.MerkleRoot(), table[i+1].expectedHash) != 0 {
			t.Errorf("error: expected hash equal to %v got %v", table[i+1].expectedHash, tree.MerkleRoot())
		}
	}
}

func Test_VerifyTree(t *testing.T) {
	for i := 0; i < len(table); i++ {

		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}

		v1, err := tree.VerifyTree()
		if err != nil {
			t.Fatal(err)
		}
		if v1 != true {
			t.Error("error: expected tree to be valid")
		}

		tree.Root.Hash = []byte{1}
		tree.merkleRoot = []byte{1}
		v2, err := tree.VerifyTree()
		if err != nil {
			t.Fatal(err)
		}
		if v2 != false {
			t.Error("error: expected tree to be invalid")
		}
	}
}

func Test_VerifyContent(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if len(table[i].contents) > 0 {
			v, err := tree.VerifyContent(table[i].contents[0])
			if err != nil {
				t.Fatal(err)
			}
			if !v {
				t.Error("error: expected valid content")
			}
		}
		if len(table[i].contents) > 1 {
			v, err := tree.VerifyContent(table[i].contents[1])
			if err != nil {
				t.Fatal(err)
			}
			if !v {
				t.Error("error: expected valid content")
			}
		}
		if len(table[i].contents) > 2 {
			v, err := tree.VerifyContent(table[i].contents[2])
			if err != nil {
				t.Fatal(err)
			}
			if !v {
				t.Error("error: expected valid content")
			}
		}
		if len(table[i].contents) > 0 {
			tree.Root.Hash = []byte{1}
			tree.merkleRoot = []byte{1}
			v, err := tree.VerifyContent(table[i].contents[0])
			if err != nil {
				t.Fatal(err)
			}
			if v {
				t.Error("error: expected invalid content")
			}
			if err := tree.RebuildTree(); err != nil {
				t.Fatal(err)
			}
		}
		v, err := tree.VerifyContent(TestHash{x: "NotInTestTable"})
		if err != nil {
			t.Fatal(err)
		}
		if v {
			t.Error("error: expected invalid content")
		}
	}
}

func Test_String(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		if tree.String() == "" {
			t.Error("error: expected not empty string")
		}
	}
}

func Test_MerklePath(t *testing.T) {
	for i := 0; i < len(table); i++ {
		tree, err := NewTree(table[i].contents)
		if err != nil {
			t.Error("error: unexpected error:  ", err)
		}
		for j := 0; j < len(table[i].contents); j++ {
			merklePath, index, _ := tree.GetMerklePath(table[i].contents[j])

			hash, err := tree.Leafs[j].calculateNodeHash()
			if err != nil {
				t.Error("error: calculateNodeHash error:  ", err)
			}
			h := sha256.New()
			for k := 0; k < len(merklePath); k++ {
				if index[k] == 1 {
					hash = append(hash, merklePath[k]...)
				} else {
					hash = append(merklePath[k], hash...)
				}
				if _, err := h.Write(hash); err != nil {
					t.Error("error: Write error:  ", err)
				}
				hash, err = calHash(hash)
				if err != nil {
					t.Error("error: calHash error:  ", err)
				}
			}
			if bytes.Compare(tree.MerkleRoot(), hash) != 0 {
				t.Errorf("error: expected hash equal to %v got %v", hash, tree.MerkleRoot())
			}
		}
	}
}
