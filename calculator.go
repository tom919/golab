package main

import (
	"fmt"
	"math"
)

func main() {
	var v1, v2, res float64

	fmt.Printf("enter variable 1:")
	_, err := fmt.Scan(&v1)
	if err != nil {
		fmt.Printf("error %v\n", err)
	}

	var ops string
	fmt.Println("select operation:")
	fmt.Scanf("%s", &ops)

	fmt.Printf("enter variable 2:")
	_, err = fmt.Scan(&v2)
	if err != nil {
		fmt.Printf("error %v\n", err)
	}

	switch ops {
	case "multiply":
		res = v1 * v2
		fmt.Printf("result %v\n", res)
	case "add":
		res = v1 + v2
		fmt.Printf("result %v\n", res)
	case "sub":
		res = v1 - v2
		fmt.Printf("result %v\n", res)
	case "div":
		res = v1 / v2
		fmt.Printf("result %v\n", res)
	case "ceil":
		res = math.Ceil(v1)
		fmt.Printf("result %v\n", res)
	case "squareroot":
		res = math.Sqrt(v1)
		fmt.Printf("result %v\n", res)
	case "floor":
		res = math.Floor(v1)
		fmt.Printf("result %v\n", res)
	case "gamma":
		res = math.Gamma(v1)
		fmt.Printf("result %v\n", res)
	case "max":
		res = math.Max(v1, v2)
		fmt.Printf("result %v\n", res)
	case "min":
		res = math.Min(v1, v2)
		fmt.Printf("result %v\n", res)
	case "pow":
		res = math.Pow(v1, v2)
		fmt.Printf("result %v\n", res)
	default:
		fmt.Printf("op didnt exsist \n")
	}
}
